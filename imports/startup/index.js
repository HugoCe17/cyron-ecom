/** Startup on Client & Server */
// import {Meteor} from 'meteor/meteor';
// import React from 'react';
// import { Route } from 'react-router';
// import Helmet from 'react-helmet';
// import { ReactRouterSSR } from 'meteor/reactrouter:react-router-ssr';
// import AppRoutes from './routes';

// ReactRouterSSR.Run(
//   // routes
//   <Route>
//     {AppRoutes}
//   </Route>,
//   // clientOptions
//   {
//     // ...
//   },
//   // serverOptions
//   {
//     htmlHook(html) {
//       const head = Helmet.rewind();
//       return html.replace('<head>', '<head>' + head.title + head.base + head.meta + head.link + head.script);
//     }
//   }
// );