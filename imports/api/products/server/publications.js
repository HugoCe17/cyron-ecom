import { Meteor } from 'meteor/meteor'
import ShopifyBuy from 'shopify-buy';
import Products from '../products'
import { uniqBy, merge, dropRight, isEqual } from 'lodash';

/** This code only runs on the server whenever a client subscribes */
Meteor.publish('products', function () {
  let inventoryCount = Products.find().count();
  let currentInventory = Products.find().fetch().sort();
  let inventory = Meteor.call('fetchAllProducts').sort();


  /** Store all Products in Mongo.Collection iff Mongo.Collection is empty*/
  if (inventoryCount === 0) {
    inventory.map((product) => {
      return Products.insert(
        merge(product.attrs,
          {
            _id: `${product.attrs.product_id}`,
            addedAt: new Date()
          }
        )
      )
    })
  }
  /** Merge older Mongo.Collection with latest fetched iff different */
  else if (!isEqual(currentInventory, inventory)) {
    let newAndOldInventory = [];
    inventory.map((product) => {
      return newAndOldInventory.push(merge(product.attrs,
        { _id: `${product.attrs.product_id}`, addedAt: new Date() }))
    })
    currentInventory.map((product) => {
      return newAndOldInventory.push(product)
    })

    let uniqued = uniqBy(newAndOldInventory, 'product_id').sort();

    /** Add to Mongo.Collection everything at the left of the array */
    let toAdd = dropRight(uniqued, currentInventory.length)
    toAdd.map((product) => Products.insert(product))
  }
  // }, 60000)

  return Products.find()

});
