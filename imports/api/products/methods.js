import { Meteor } from 'meteor/meteor'
import Products from './products.js'
import ShopifyBuy from 'shopify-buy'

const ShopClient = ShopifyBuy.buildClient({
  apiKey: '22f8cf2b78da26bc3b9eec488a180f12',
  myShopifyDomain: 'just-stories',
  appId: '6',
});

Meteor.methods({
  /** fetch inventory from Shopify promise */
  fetchAllProducts: () => {
    return Promise.await(
      ShopClient.fetchAllProducts()
        .then((_products) => _products)
        .catch(() => console.log("Fetch all Products Error"))
    )
  }
})