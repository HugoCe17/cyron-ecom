import { DocHead } from 'meteor/kadira:dochead';
import React, { Component } from 'react';

import injectTapEventPlugin from "react-tap-event-plugin";
injectTapEventPlugin();

const metaTags = [
  { name: 'google-site-verification', content: 'QNU8XLUa2IaBCpjlll0tnceOlka5-aoH7fWSkbnTz4U' },
  { name: 'msapplication-TileColor', content: '#ffffff' },
  { name: 'msapplication-TileImage', content: '/ms-icon-144x144.png' },
  { name: 'theme-color', content: '#ffffff' },
  { name: 'keywords', content: 'Ecommerce Stack with Meteor, React, and lots of love!' },
  { name: 'apple-mobile-web-app-capable', content: 'yes' },
  { name: 'description', content: 'Implementing a Stack ready to create Shopify Buy Driven UIs, that is, we\'re building great custom Ecommerce with the latest technologies in web development/engineering' },
  { name: 'viewport', content: 'width=device-width, initial-scale=0.8, minimum-scale=0.8, maximum-scale=0.8, user-scalable=0' },
];

class Layout extends Component {

  getAllMetaTags() {
    DocHead.setTitle('MeteorEcom');
    return metaTags.map((meta) => DocHead.addMeta(meta)
    );
  }

  render() {
    return (
      <div>
        <header>
          {this.getAllMetaTags() }
        </header>
        <main>
          {this.props.content}
        </main>
      </div>
    );
  }
}

export default Layout;