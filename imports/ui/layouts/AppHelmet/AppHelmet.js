import React from 'react';
import Helmet from 'react-helmet';

// import injectTapEventPlugin from "react-tap-event-plugin";
// injectTapEventPlugin();

// let tempArr = [
//   { rel: "apple-touch-icon", sizes: "57x57", href: "./io-favi/apple-icon-57x57.png" },
//   { rel: "apple-touch-icon", sizes: "60x60", href: "./io-favi/apple-icon-60x60.png" },
//   { rel: "apple-touch-icon", sizes: "72x72", href: "./io-favi/apple-icon-72x72.png" },
//   { rel: "apple-touch-icon", sizes: "76x76", href: "./io-favi/apple-icon-76x76.png" },
//   { rel: "apple-touch-icon", sizes: "114x114", href: "./io-favi/apple-icon-114x114.png" },
//   { rel: "apple-touch-icon", sizes: "120x120", href: "./io-favi/apple-icon-120x120.png" },
//   { rel: "apple-touch-icon", sizes: "144x144", href: "./io-favi/apple-icon-144x144.png" },
//   { rel: "apple-touch-icon", sizes: "152x152", href: "./io-favi/apple-icon-152x152.png" },
//   { rel: "apple-touch-icon", sizes: "180x180", href: "./io-favi/apple-icon-180x180.png" },
//   { rel: "icon", type: "image/png", sizes: "192x192", href: "./io-favi/android-icon-192x192.png" },
//   { rel: "icon", type: "image/png", sizes: "32x32", href: "./io-favi/favicon-32x32.png" },
//   { rel: "icon", type: "image/png", sizes: "96x96", href: "./io-favi/favicon-96x96.png" },
//   { rel: "icon", type: "image/png", sizes: "16x16", href: "./io-favi/favicon-16x16.png" },
//   { rel: "manifest", href: "./io-favi/manifest.json" },
// ]

export default function LandingPageHelmet({ children }) {
  return (
    <div>
      <Helmet
        title="EcomMeteor"
        defaultTitle="EcomMeteor"
        meta={[
          { name: "google-site-verification", content: "QNU8XLUa2IaBCpjlll0tnceOlka5-aoH7fWSkbnTz4U" },
          { name: "msapplication-TileColor", content: "#ffffff" },
          { name: "msapplication-TileImage", content: "/ms-icon-144x144.png" },
          { name: "theme-color", content: "#ffffff" },
          { name: "keywords", content: 'Meteor Ecommerce' },
          { name: "apple-mobile-web-app-capable", content: "yes" },
          { name: 'description', content: 'Ecommerce with Meteor and Shopify' },
          { name: "apple-mobile-web-app-status-bar-style", content: "black-translucent" },
          { name: 'viewport', content: 'width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0' },
        ]}
        link={[
          { href: "https://fonts.googleapis.com/icon?family=Material+Icons", rel: "stylesheet" },
          { href: "https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css", rel: "stylesheet", },
          { href: 'https://fonts.googleapis.com/css?family=Varela', rel: 'stylesheet', type: 'text/css', },
        ]}
      />
      {children}
    </div>
  )
}