import React, { Component, PropTypes } from 'react';
import { findDOMNode } from 'react-dom'

/** CyronIO Components */
import HeroWelcome from '../../cyronco/HeroWelcome'
import Navbar from '../../cyronco/Navbar';
import CentredText from '../../cyronco/CentredText'
import Footer from '../../cyronco/Footer'

/** Ecom Components */
import Products from '../../ecomco/Products'
import GridedProducts from '../../ecomco/GridedProducts'


/** Mui Theming */
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import lightBaseTheme from './AppThemes'

const muiTheme = getMuiTheme(lightBaseTheme, {
  avatar: {},
  userAgent: 'all',
})

class App extends Component {
  componentDidMount() {
    $('html, body').niceScroll({
      scrollSpeed: 70,
    });
  }

  render() {
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <div ref="appBody">
          <Navbar />
          <HeroWelcome type="parallax" src="/images/mac-iphone-glasses.jpg" bgMargins="0px" />
          <GridedProducts id="root" />

        </div>
      </MuiThemeProvider>
    )
  }
}


export default App;
