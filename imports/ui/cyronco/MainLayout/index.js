import { DocHead } from 'meteor/kadira:dochead';
import React, { Component } from 'react';

const metaTags = [
  { name: 'google-site-verification', content: 'QNU8XLUa2IaBCpjlll0tnceOlka5-aoH7fWSkbnTz4U' },
  { name: 'msapplication-TileColor', content: '#ffffff' },
  { name: 'msapplication-TileImage', content: '/ms-icon-144x144.png' },
  { name: 'theme-color', content: '#ffffff' },
  { name: 'keywords', content: 'Hialeah, Hialeah Web Development, Miami, Miami Web Development, Web Development, Broward, Broward Web Development, Kendall, Kendall Web Development, Miami Mobile Development, Broward Mobile Development, Hialeah Mobile Development, Kendall Mobile Development, Information Technology, Information Technology Consultation, IT, IT Consultation, Camera, Installation, Surveillance, Surveillance Camera Installation, Network Installation, Android, iOS, Android Development, iOS Development, React, Angular, Meteor, ReactJS, AngularJS, MeteorJS, Node, NodeJS, Web, Development, Software Development, Miami Software Development, Broward Software Development, Kendall Software Development, Software Consultation, Miami Software Development, Hialeah Software Development, Broward Software Development, Miami Software Consultation, Broward Software Consultation, Fullstack, Full-stack, DevOps, Developer Operations, LLC, Cyron I/O, Cyron I/O LLC' },
  { name: 'apple-mobile-web-app-capable', content: 'yes' },
  { name: 'description', content: 'Cyron I/O, A Software and IT Company based in Miami looking to entice more local businesses onto the idea that having a digital footprint is crucial in remaining competetive. Cyron I/O hopes to leverage modern technologies to solve problems for businesses to help them improve their already existing digital footprint, or creating one' },
  { name: 'apple-mobile-web-app-status-bar-style', content: 'black-translucent' },
  { name: 'viewport', content: 'width=device-width, initial-scale=0.8, minimum-scale=0.8, maximum-scale=0.8, user-scalable=0' },
];

class MainLayout extends Component {

  getAllMetaTags() {
    DocHead.setTitle('Cyron I/O');
    return metaTags.map((meta) => DocHead.addMeta(meta)
    );
  }

  render() {
    return (
      <div>
        <header>
          {this.getAllMetaTags() }
        </header>
        <main>
          {this.props.content}
        </main>
      </div>
    );
  }
}

export default MainLayout;