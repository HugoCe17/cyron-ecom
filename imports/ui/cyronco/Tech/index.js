import React, {Component, PropType} from 'react'
import Slider from 'react-slick'

const styles = {
    main: {
        positition: 'relative',
        background: "white",
        width: "80%",
        padding: "10px"
    },
    h1: {
        fontSize: "36px",
        margin: "36px 5px "
    },

    wrapper: {
        width: "100vw",
        background: "white"
    }
}

export default class Tech extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        if (typeof window !== 'undefined') {
            window.setTimeout(() => window.innerWidth + 1, 100)
        }
    }

    renderImages() {
        return this.props.data.map((str, index, array) => {
            return (index >= 3 && index <= 6)
                ? (
                    <img
                        style={ {
                            paddingTop: "46px",
                        } }
                        key={str}
                        className="img-responsive"
                        src={str} />
                )
                : (
                    <img
                        key={str}
                        className="img-responsive"
                        src={str} />
                )
        })
    }

    render() {
        let settings = {
            arrows: false,
            dots: false,
            speed: 700,
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            adaptiveHeight: false
        }

        return (
            <section style={styles.wrapper}>
                <div
                    className="container"
                    ref="slide"
                    style={ styles.main }>
                    <div
                        style={{ marginBottom: "25px" }}
                        className="row">
                        <h1
                            className='text-center'
                            style={styles.h1} >
                            Showcase
                        </h1>
                        <Slider {...settings}>
                            {this.renderImages() }
                        </Slider>
                    </div>
                </div>
            </section>

        )
    }

}
