import React, { Component, PropTypes } from 'react'
import CentredText from '../CentredText'
import RaisedButton from 'material-ui/RaisedButton'


const styles = {
    rowCentered: { textAlign: 'center' },
    colCentered: {
        display: 'inlineBlock',
        float: 'none',
        /* reset the text-align */
        textAlign: 'left'
    }

}
export default class Packages extends Component {
    render() {
        return (
            <div
                style={{ background: "white", paddingBottom: "20px" }}
                className="packages row">
                <CentredText
                    heading="Choose a plan"
                    icon=" icon-genius"
                    id="Packages"
                    />
                <div
                    style={styles.colCentered}
                    className="container" >
                    <div
                        className="row"
                        style={styles.rowCentered}>
                        {this.renderPacks() }
                    </div>
                </div>
            </div>
        )
    }

    renderPacks() {
        return this.props.data.map((obj) => {
            return (
                <Pack
                    key={obj.title}
                    icon={obj.icon}
                    title={obj.title}
                    text={obj.text}
                    />
            )
        })
    }
}

/////////////////////
//      PROPS
//-icon
//-title
//-text

const style = {
    pack: {
        padding: "25px 40px",
        border: "1px solid #eee",
        margin: '10px'
    },

    p: {
        fontSize: "14px",
        marginBottom: "20px",
        width: '200px'
    },

    icon: {
        display: "block",
        fontSize: "80px",
        lineHeight: "80px",
        marginBottom: "10px"
    },

    sup: {
        verticalAlign: "top",
        top: "10px",
        fontSize: "20px"
    }
}

// class Pack extends Component {
function Pack({icon, title, text}) {
    return (
        <div
            className="hvr-bounce-to-top text-center pack"
            style={ style.pack }
            >
            <span
                style={style.icon}
                className={icon}></span>
            <h3>{title}</h3>
            <br/>
            <p style={style.p }>{text}</p>
            <RaisedButton
                label="Learn More"
                labelColor="white"
                backgroundColor="rgba(98%, 37.8%, 30.9%, 1)"
                />
        </div>
    )
}