import React from 'react';
import Helmet from 'react-helmet';

var injectTapEventPlugin = require("react-tap-event-plugin");
injectTapEventPlugin();
export default function LandingPageHelmet({ children }) {
  return (
    <div>
      <Helmet
        title="Cyron I/O"
        defaultTitle="Cyron I/O"
        meta={[
          { name: "google-site-verification", content: "QNU8XLUa2IaBCpjlll0tnceOlka5-aoH7fWSkbnTz4U" },
          { name: "msapplication-TileColor", content: "#ffffff" },
          { name: "msapplication-TileImage", content: "/ms-icon-144x144.png" },
          { name: "theme-color", content: "#ffffff" },
          { name: "keywords", content: 'Hialeah, Hialeah Web Development, Miami, Miami Web Development, Web Development, Broward, Broward Web Development, Kendall, Kendall Web Development, Miami Mobile Development, Broward Mobile Development, Hialeah Mobile Development, Kendall Mobile Development, Information Technology, Information Technology Consultation, IT, IT Consultation, Camera, Installation, Surveillance, Surveillance Camera Installation, Network Installation, Android, iOS, Android Development, iOS Development, React, Angular, Meteor, ReactJS, AngularJS, MeteorJS, Node, NodeJS, Web, Development, Software Development, Miami Software Development, Broward Software Development, Kendall Software Development, Software Consultation, Miami Software Development, Hialeah Software Development, Broward Software Development, Miami Software Consultation, Broward Software Consultation, Fullstack, Full-stack, DevOps, Developer Operations, LLC, Cyron I/O, Cyron I/O LLC' },
          { name: "apple-mobile-web-app-capable", content: "yes" },
          { name: 'description', content: 'Cyron I/O, A Software and IT Company based in Miami looking to entice more local businesses onto the idea that having a digital footprint is crucial in remaining competetive. Cyron I/O hopes to leverage modern technologies to solve problems for businesses to help them improve their already existing digital footprint, or creating one' },
          { name: "apple-mobile-web-app-status-bar-style", content: "black-translucent" },
          { name: 'viewport', content: 'width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0' }
        ]}
        link={[
          { rel: "apple-touch-icon", sizes: "57x57", href: "./io-favi/apple-icon-57x57.png" },
          { rel: "apple-touch-icon", sizes: "60x60", href: "./io-favi/apple-icon-60x60.png" },
          { rel: "apple-touch-icon", sizes: "72x72", href: "./io-favi/apple-icon-72x72.png" },
          { rel: "apple-touch-icon", sizes: "76x76", href: "./io-favi/apple-icon-76x76.png" },
          { rel: "apple-touch-icon", sizes: "114x114", href: "./io-favi/apple-icon-114x114.png" },
          { rel: "apple-touch-icon", sizes: "120x120", href: "./io-favi/apple-icon-120x120.png" },
          { rel: "apple-touch-icon", sizes: "144x144", href: "./io-favi/apple-icon-144x144.png" },
          { rel: "apple-touch-icon", sizes: "152x152", href: "./io-favi/apple-icon-152x152.png" },
          { rel: "apple-touch-icon", sizes: "180x180", href: "./io-favi/apple-icon-180x180.png" },
          { rel: "icon", type: "image/png", sizes: "192x192", href: "./io-favi/android-icon-192x192.png" },
          { rel: "icon", type: "image/png", sizes: "32x32", href: "./io-favi/favicon-32x32.png" },
          { rel: "icon", type: "image/png", sizes: "96x96", href: "./io-favi/favicon-96x96.png" },
          { rel: "icon", type: "image/png", sizes: "16x16", href: "./io-favi/favicon-16x16.png" },
          { rel: "manifest", href: "./io-favi/manifest.json" },
          { href: "https://fonts.googleapis.com/icon?family=Material+Icons", rel: "stylesheet" },
          { rel: "stylesheet", href: "https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" },
          { href: 'https://fonts.googleapis.com/css?family=Varela', rel: 'stylesheet', type: 'text/css' }
        ]}
        />
      {children}
    </div>
  )
}