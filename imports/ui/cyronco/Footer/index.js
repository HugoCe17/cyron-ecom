import React from 'react'

const styles = {
  bottomFade: {
    zIndex: '10000',
    position: 'absolute',
    bottom: 0,
    width: "100%",
    height: "80px",
    background: `1920px 112px url(${'./bottom-fade.png'})`
  }

}

export default function Footer() {
  return (
    <div>
      <section className="footer footer-top text-center">
        <div className="fade-bottom-light"/>
        <div className="container">
          <div className="row">
            <div
              className="col-md-4"
              style={{ margin: '0 115px' }}>
              <h1>Cyron I/O</h1>
              <p>Quality software created by quality people, for local businesses.</p>
            </div>

            <div className="col-md-4">
              <div className="">
                <h1>Reach Us</h1>
                <ul>
                  <li><i className="fa fa-map"></i> Miami, Florida</li>
                  <li><i className="fa fa-mobile"></i> (305) 790-7719</li>
                </ul>
              </div>
            </div>
            {/**                
              <div className="col-sm-4 col-sm-offset-4">
                  <h3>Social Media</h3>
                  <ul className="social">
                      <li className="facebook">
                          <a href="#"> <i className="fa fa-facebook"></i> </a>
                      </li>
                      <li className="twitter">
                          <a href="#"> <i className="fa fa-twitter"></i> </a>
                      </li>
                      <li className="google-plus">
                          <a href="#"> <i className="fa fa-google-plus"></i> </a>
                      </li>

                      <li className="linkedin">
                          <a href="#"> <i className="fa fa-linkedin"></i> </a>
                      </li>
                      <li className="youtube">
                          <a href="#"> <i className="fa fa-youtube-play"></i> </a>
                      </li>

                  </ul>
              </div>
              */}
          </div>
        </div>
      </section>
      <footer className="footer footer-sub text-center">
        <div className="fade-bottom-dark"/>
        <div className="container">
          <div className="row">
            <div className="col-lg-6 col-sm-6">
              <p>&copy; Cyron I/O, LLC.All Rights Reserved</p>
            </div>
          </div>
        </div>
      </footer>
    </div>
  )

}
