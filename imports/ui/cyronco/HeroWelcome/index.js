import React, { Component, PropTypes } from 'react';
import { Parallax, Background } from 'react-parallax';
import $ from 'jquery';

const style = {
  section: {
    height: '100vh',
    width: '100vw',
  },
  h4: {
    textTransform: 'uppercase',
    fontWeight: '300',
    fontSize: '25px',
    color: 'white',
  },
  h3: {
    fontWeight: '700',
    color: 'gray',
  },
  h1: {
    textTransform: 'capitalize',
    fontSize: '45px',
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  p: {
    fontSize: '20px',
    color: 'white',
  },
  form: {
    background: 'rgb(245, 245, 245)',
    opacity: 0.9,
    borderRadius: '5px',
    padding: '25px 10px',
    width: 'auto',
    margin: '',
  },
  btn: {
    width: '150px',
    marginTop: '15px',
  },
  video: {
    zIndex: '-1000',
    position: 'fixed',
    top: '50%',
    left: '50%',
    minWidth: '100%',
    minHeight: '100%',
    width: 'auto',
    height: 'auto',
    msTransform: 'translateX(-50%) translateY(-50%)',
    MozTransform: 'translateX(-50%) translateY(-50%)',
    WebkitTransform: 'translateX(-50%) translateY(-50%)',
    transform: 'translateX(-50%) translateY(-50%)',
  },
};

export default class HeroWelcome extends Component {
  constructor(props) {
    super(props);
    this.state = { animation: '' };
    this.applyType = this.applyType.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.renderOverlay = this.renderOverlay.bind(this);
  }

  componentDidMount() {
    /** Ceremony for getting react-parallax to play nice */
    $(document).ready(() => {
      if (this.props.type === 'parallax') {
        const $parallax = $('.parallax-hc .react-parallax');
        const $parallaxContent = $('.parallax-hc .react-parallax-content');
        const $parallaxBGChildren = $('.parallax-hc .react-parallax-background-children');
        $parallax.css({ height: '100vh' });
        $parallaxContent.css({ height: '100vh' });
        $parallaxBGChildren.css({ transform: 'translate3d(-50%, -271px, 0px)' });
      }
    });
  }

  handleClick() {
    const $nav = $('.nav').height() - 110;
    $('#main0').scrollTo($nav); // id should be passed in through prop
  }

  applyType(type) {
    switch (type) {
      case 'parallax':
        return (
          <div
            className="parallax-hc animated fadeIn"
            style={{ animationDelay: '2s' }}
          >
            <Parallax strength={300}>
              <Background>
                <img
                  src={this.props.src}
                  alt=""
                  style={{
                    position: 'relative',
                    height: '100%',
                    WebkitFilter: 'blur(1px)',
                    filter: 'blur(1px)',
                  }}
                />
              </Background>
              {this.renderOverlay()}
            </Parallax>
          </div>
        );
      case 'video':
        return (
          <div id="video-hero">
            <div className={"container animated fadeIn"} style={style.video}>
              <video id="video" autoPlay muted >
                <source src={this.props.src} />
              </video>
            </div>
            {this.renderOverlay()}
          </div>
        );
      default:
        return null;
    }
  }

  renderOverlay() {
    return (
      <div
        className="animated fadeInDown text-center"
        style={{
          position: 'relative',
          top: '25vh',
          transform: 'translateY(-50%)',
          WebkitAnimationDelay: '2.5s',
          animationDelay: '2.5s',
        }}
      >

        <div className="col-md-6 col-md-offset-3">
          <h1 style={style.h1} >
            Software & IT Solutions
          </h1>
        </div>

        <div className="" >
          <div
            className="col-md-12"
            onClick={this.handleClick}
            style={{
              paddingTop: '160px',
              fontSize: '60px',
              color: 'white',
            }}
          >
            <i
              className="fa fa-angle-double-down"
              aria-hidden="true"
            >
            </i>
          </div>

        </div>
      </div>
    );
  }

  render() {
    return (
      <div
        id="bannerVideo"
        style={style.section}
      >
        {this.applyType(this.props.type)}
      </div>
    );
  }

}

HeroWelcome.propTypes = {
  type: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  bgMargin: PropTypes.string,
};

HeroWelcome.defaultProps = {
  bgMargin: '0px',
};
