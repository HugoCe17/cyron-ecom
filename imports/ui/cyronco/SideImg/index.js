import React, {Component, PropTypes} from 'react'
import CyronAccordion from '../Accordion'


const style = {
   h1: {
      textTransform: "capitalize",
      fontWeight: "500",
      fontSize: "36px",
      marginBottom: "15px",
      bottom: '40px',
      textAlign: 'center',
      color: "#4F5692"
   },

   small: {
      display: "block",
      fontSize: "20px",
      lineHeight: "30px",
      color: "#4F5692",
      padding: "12px 0",

   },

   processH1: {
      textTransform: "capitalize",
      fontWeight: "500",
      fontSize: "60px",
      marginBottom: "15px",
      textAlign: "center",
      padding: '15px 0 15px 0',
      color: "#4F5692"
   },

   img: {
      position: 'relative',
      margin: '15px 0 20px 0',
      left: '-25px'
   }
}

export default class SideImg extends Component {
   constructor(props) {
      super(props)

      this.state = {
         adjacentEl: ''
      }

      this.chooseCase = this.chooseCase.bind(this)
   }


   render() {
      // show header if accordian enabled
      let header = this.props.accordian
         ? (
            <h1 style={style.processH1 }>
               The Process
            </h1>
         )
         : null

      return (
         <div style={{
            width: "100%",
            background: this.props.backgroundColor,
            padding: this.props.padding
         }}>
            <div className="row">
               { header }
            </div>
            { this.chooseCase() }
         </div>
      )
   }

   chooseCase() {
      let { float } = this.props

      let adjacentEl =
         (this.props.accordian)
            ?
            <div className="col-md-6">
               <CyronAccordion title="" />
            </div>
            :
            <div className="col-md-6">
               <h1 style={style.h1}>
                  {this.props.heading}
               </h1>
               <small style={style.small}>
                  {this.props.small}
               </small>
            </div>

      return (float === 'right')
         ? <div className="sideimg container text-center">

            {adjacentEl}

            <div className="col-md-6">
               <img
                  className="animated fadeInRight img-responsive"
                  src={ this.props.img }
                  alt=""
                  />
            </div>
         </div>
         :
         <div className="container text-center">

            <div className="col-md-6">
               <img
                  className="animated fadeInLeft img-responsive"
                  src={this.props.img}
                  style={style.img}
                  alt=""
                  />
            </div>

            {adjacentEl}
         </div>

   }
}

SideImg.propTypes = {
   backgroundColor: PropTypes.string,
   accordian: PropTypes.bool
}
SideImg.defaultProps = {
   backgroundColor: '#fff',
   accordian: false
}