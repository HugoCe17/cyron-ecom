import React, { Component, PropTypes } from 'react'


const style = {
    ul: {
        margin: "30px",
        paddingBottom: "30px",
        borderBottom: "1px solid #eee",
        color: "#4F5692"
    },

    icon: {
        fontSize: "30px",
        paddingBottom: "20px"
    },
}


export default class Skills extends Component {
    constructor(props) {
        super(props)

        this.state = {
            selected: 0,
            toggleAnimation: 'fadeInLeft',
            toggleDisplay: 'block'
        }

        this.handleClick = this.handleClick.bind(this)

    }


    render() {
        return (
            <div id="skills">
                <div
                    style={{ background: "white" }}
                    className="row">
                    <div className="col-sm-8 col-sm-offset-2">
                        <ul
                            style={style.ul}
                            className="list-inline text-center tabs-navigation"
                            role="tablist">
                            {this.renderOptions() }
                        </ul>
                    </div>
                </div>
                {this.renderComponent() }
            </div>
        )
    }

    renderComponent() {
        switch (this.state.selected) {
            case 0:
                return (
                    <SideImgMod
                        display={this.state.toggleDisplay}
                        animation={this.state.toggleAnimation}
                        float="left"
                        heading="UI/UX Design"
                        img="./design.jpg"
                        small="The design of the user interface and user experience, formally known as UI/UX Design, is the art of designing with the user in mind. Users over everything. HTML5's sole philosophy is based around the user and so should your website. We pride ourselves in starting and ending with your user and pushing our skills to satisfy their needs."/>
                )
            case 1:
                return (
                    <SideImgMod
                        display={this.state.toggleDisplay}
                        animation={this.state.toggleAnimation}
                        float="right"
                        heading="Responsive Design"
                        img="./responsive.png"
                        small="Technology is facing exponential growth and your business shouldn't be left behind. With our responsive design, your website will be just as beautiful regardless of the device it is viewed. We will deliver an immersive experience that captures your user's attention on mobile, tablet and desktop."/>
                )
            case 2:
                return (
                    <SideImgMod
                        display={this.state.toggleDisplay}
                        animation={this.state.toggleAnimation}
                        float="left"
                        heading="Web Development"
                        img="./development.jpg"
                        small="Web development can range from developing the simplest static single page of plain text to the most complex web-based internet applications, electronic businesses, and social network services, we prefer the latter. Our group of developers stay in touch with the latest technologies in order to make a fast and secure web application."/>
                )
            case 3: return (
                <SideImgMod
                    display={this.state.toggleDisplay}
                    animation={this.state.toggleAnimation}
                    float="right"
                    heading="SEO"
                    img="./seo.jpg"
                    small="SEO stands for “search engine optimization.” It is the process of getting traffic from the “free,” “organic,” “editorial” or “natural” search results on search engines. Our content writer will not just tell a story for your users but will also cleverly optimize the content for greater search hits."/>
            )
        }
    }

    handleClick(id) {

        _self = this // pointer reference to this

        const _id = id
        const mapAnimations = [
            'fadeInRight',
            'fadeInLeft',
            'fadeInUp',
            'fadeInRightBig',
            'fadeInLeftBig',
            'fadeInUpBig'
        ]

        let randAnimation =
            mapAnimations[Math.floor(Math.random() * mapAnimations.length)]

        this.setState({
            toggleAnimation: 'fadeOut'
        })

        let timeInBetweenTransisition = 400
        if (typeof window !== 'undefined') { // guard against serversiderender error
            setTimeout(function () {
                _self.setState({
                    selected: _id,
                    toggleAnimation: randAnimation // 0 based mapping
                })
            }, timeInBetweenTransisition)
        }

    }


    renderOptions() {
        return this.props.data.map((obj) => {
            return (
                <Option
                    selected={this.state.selected}
                    onClick={this.handleClick}
                    id={obj.id}
                    key={obj.id}
                    title={obj.title}
                    icon={obj.icon} />
            )
        })
    }
}


//This the component that is used in the Map function of the parent component Skills to create the buttons

class Option extends Component {
    constructor(props) {
        super(props)

        this.state = {
            selected: this.props.selected,
            isMobile: typeof window !== 'undefined' ? window.mobilecheck() : null
        }

        this.onClick = this.onClick.bind(this)
    }

    render() {
        return (
            <li
                onClick={this.onClick}
                style={this.getCss() }>
                <span
                    style={style.icon}
                    className={this.props.icon}>
                </span>
                <p>{this.props.title}</p>
            </li>
        )
    }

    getCss() {
        if (this.props.id === this.props.selected) {
            return {
                padding: "10px",
                color: "salmon",
                boxShadow: "0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)",
                borderBottom: "1px solid salmon"
            }
        }

        return {
            padding: "20px"
        }
    }

    onClick() {
        this.setState({ selected: this.props.id })
        this.props.onClick(this.props.id)
        let $nav = $(".nav").height()

        if (this.state.isMobile) {
            return $("#mobiletarget").scrollTo($nav)
        }

        return $('#skills').scrollTo($nav)
    }
}

//Below are the 4 Components that get rendered depending on the state of Skills (0,1,2,3)
//0 - UI/UX
//1 - Responsive Design
//2 - Web Development
//3 - Seo

//FullWidth SideImg
class SideImgMod extends Component {
    constructor(props) {
        super(props)
        this.state = {
            animation: '',
            display: ''
        }

        this.chooseCase = this.chooseCase.bind(this)
    }


    componentWillMount() {
        this.setState({
            animation: this.props.animation,
            display: this.props.display
        })
    }


    render() {
        return (
            <div id="mobiletarget" style={{
                display: this.state.display,
                width: "100vw",
                background: "white",
                padding: "15px 0 126px 0"
            }}>
                { this.chooseCase() }
            </div>
        )
    }


    chooseCase() {
        let { float } = this.props

        return (float === 'right')
            ? <div className=" container  text-center">
                <div className={`animated ${this.props.animation} col-md-6`}>
                    <h1 style={this.getCssH1() }>{this.props.heading}</h1>
                    <small style={this.getCssSmall() }>{this.props.small}</small>

                </div>
                <div className={`animated ${this.props.animation} col-md-6`}>
                    <img className="img-responsive" src={this.props.img} alt=""/>
                </div>
            </div>
            :
            <div className="container text-center">
                <div className={`animated ${this.props.animation} col-md-6`}>
                    <img className="img-responsive" src={this.props.img} alt=""/>
                </div>
                <div className={`animated ${this.props.animation} col-md-6`}>
                    <h1 style={this.getCssH1() }>{this.props.heading}</h1>
                    <small style={this.getCssSmall() }>{this.props.small}</small>
                </div>
            </div>

    }

    getCssH1() {
        return {
            textTransform: "capitalize",
            fontWeight: "500",
            fontSize: "36px",
            marginBottom: "15px",
            bottom: '40px',
            textAlign: 'center',
            color: "#4F5692"
        }
    }

    getCssSmall() {
        return {
            fontSize: "20px",
            lineHeight: "30px",
            color: "#4F5692"
        }
    }
}