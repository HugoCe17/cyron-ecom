import React, {Component, PropTypes} from 'react'

import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'



const style = {
    form: {
        background: "rgb(245, 245, 245)",
        opacity: 0.9,
        borderRadius: "5px",
        padding: "25px 10px",
        width: "auto"
    },
    h3: {

    },
    btn: {
        width: "150px",
        marginTop: "15px"
    },
    img1: {
        position: "absolute",
        height: "50%",
        width: "100%",
        top: "0px",
        border: "0px",
    },
    img2: {
        position: "absolute",
        height: "50%",
        width: "100%",
        bottom: 0,
        right: 0,
    },
}


export default function SplitImg({ img1, img2 }) {
    return (
        <div
            className="row"
            style={{
                position: "relative",
                width: "auto",
                paddingTop: "40px",
                paddingBottom: "40px"
            }}>
            <div
                style={ _.extend({}, style.img1, {
                    background: `url(${img1}) no-repeat 15px -660px / 2400px`
                }) } />

            <div
                style={_.extend({}, style.img2, {
                    background: `url(${img2}) no-repeat 15px -613px / 2200px`
                }) } />

            <FormMod />
        </div>
    )
}

class FormMod extends Component {
    render() {
        return (
            <div className="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 text-center">
                <div
                    style={style.form}
                    className="container">
                    <h1 style={style.h3}>
                        Free Consultation
                    </h1>
                    <form
                        onSubmit={this._handleSubmit}
                        onChange={this.clearErrors}>

                        <div cassName="">
                            <TextField
                                onChange={ (event) => {
                                    this.setState({
                                        FullName: event.target.value
                                    })
                                } }
                                ref="fullName"
                                name="fullName"
                                required={true}
                                type="text"
                                floatingLabelText="Full Name"
                                />
                            <br/>

                            <TextField
                                onChange={ (event) => {
                                    this.setState({
                                        Email: event.target.value
                                    })
                                } }
                                ref="email"
                                name="Email"
                                required={true}
                                type="Email"
                                floatingLabelText="Email"
                                />
                            <br/>
                            <TextField
                                multiLine={false}
                                floatingLabelText="Contact us now!"
                                />
                            <br/>
                            <RaisedButton
                                className="text-center"
                                backgroundColor="#f77"
                                label="Submit"
                                primary={true}
                                style={style.btn}
                                />

                        </div>
                    </form>
                </div>
            </div>
        )
    }
}