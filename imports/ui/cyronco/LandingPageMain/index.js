import React, { Component, PropTypes } from 'react'
import $ from 'jquery'
import Navbar from '../Navbar'
import HeroWelcome from '../HeroWelcome'
import CentredText from '../CentredText'
import CyronParallax from '../Parallax'
import SideImg from '../SideImg'
import ShowCase from '../ShowCase'
import Packages from '../Packages'
import Skills from '../Skills'
import TheProcess from '../TheProcess'
import Footer from '../Footer'
import SplitImg from '../SplitImg'
import Tech from '../Tech'
import CyronShowcase from '../CyronShowcase'
// import Notification from '../Notification'
import GoogleMaps from '../GoogleMaps'

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import cyronTheme from '../../../CyronRawTheme'


const dataPackages = [{
  id: 0,
  icon:
  'icon-phone',
  title: 'Personal',
  text: 'Take your personal brand to the next level',
},
  {
    id: 1,
    icon: 'icon-mobile',
    title: 'Business',
    text: 'Show the world your unique company culture',
  },
];

const dataSkills = [
  { id: 0, title: 'UI/UX Design', icon: 'icon-edit' },
  { id: 1, title: 'Responsive Design', icon: 'icon-search' },
  { id: 2, title: 'Web Development', icon: 'icon-gears' },
  { id: 3, title: 'Seo', icon: 'icon-globe' },
];

// const showcaseData = [
//   './SideImg/blurred.jpg',
//   './SideImg/blurred.jpg',
//   './SideImg/blurred.jpg',
//   './SideImg/blurred.jpg',
// ];

// const techData = [
//   '/techicons/css3.png',
//   '/techicons/html5.png',
//   '/techicons/js.png',
//   '/techicons/meteor.png',
//   '/techicons/node.png',
//   '/techicons/react.png',
//   '/techicons/vscode.png',
// ];

const styles = {
  body: {
    width: 'auto',
    zIndex: 100,
  },
};

const muiTheme = getMuiTheme(cyronTheme, {
  avatar: {
    borderColor: null,
  },
  userAgent: 'all',
});

export default class App extends Component {
  render() {
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <LandingPageMain />
      </MuiThemeProvider>
    );
  }
}

class LandingPageMain extends Component {
  componentDidMount() {
    if (window) {
      $('html, body').niceScroll({
        scrollSpeed: 80,
      });
    }
  }

  render() {
    return (
      <div
        ref="appRoot"
        id="app-body"
        style={styles.body}
      >

        <Navbar />

        {/** <HeroWelcome type="parallax" src="/mac-iphone-glasses.jpg" bgMargins="0px" /> */}
        <HeroWelcome type="video" src="/tech.mp4" />

        <CentredText
          heading="Why Choose Us"
          icon="icon-linegraph"
          id="main0"
        />

        <SideImg
          heading="Capture attention"
          small="You work hard to grab the attention of your customers, your website shouldn't scare them away. Rather, we believe your customers should feel at home when entering your website which is why we engineer a user experience that flawlessly immerses them into your culture."
          img="./attention.jpg"
          float="left"
          padding="85px 0px 200px"
        />

        <CyronParallax
          bgMargins={0}
          bgImage={'./beach.jpg'}
        />

        <SideImg
          heading="Leave your mark"
          small="Business in the 21st century requires an online presence. If you aren't on the web, then you are out of business. Make your presence known with our immersive landing pages, interactive e-commerce sites or our business platforms."
          img="./Mark.jpg"
          float="right"
          padding="110px 0px 90px 0px"
        />

        <CyronParallax
          bgMargins="-266px 0px 0px 0px"
          bgImage={'./beach2.jpg'}
        />

        <CentredText
          heading="What We Do"
          icon="icon-strategy"
          id="main1"
        />

        <Skills data={dataSkills} />

        <TheProcess />

        <GoogleMaps />

        {/**
        <ShowCase
          arrows={true}
          dots={true}
          show={3}
          data={showcaseData}
          auto={false}
        />
        <CyronShowcase />

        <Packages data={dataPackages} />

        <SplitImg
            img1="./orlando.jpg"
            img2="./Miamipink.jpg"
        />
        */}

        <Footer />

      </div>
    );
  }
}
