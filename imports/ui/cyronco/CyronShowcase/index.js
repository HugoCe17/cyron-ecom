typeof window !== 'undefined' ? require('./styles.import.css') : null
import { Component, PropTypes } from 'react'

const cssHtmlBootstrapImg = './logos/css3-html5-bootstrap.png'
const nodejsImg = './logos/nodejs.png'
const sassImg = './logos/sass-logo.png'
const webpackImg = './logos/webpack.png'
const reactjsImg = './logos/reactjs.svg'
const meteorImg = './logos/meteor-logo.png'

const styles = {
   meteorImgStyle: {
      position: "relative",
      width: '10rem',
      height: '100%',
      top: '18px'
   }
}

const logosPaths = [
   cssHtmlBootstrapImg,
   nodejsImg,
   meteorImg,
   reactjsImg,
   sassImg,
   webpackImg
]

// Showcase a set of BrandImages
export default class CyronShowcase extends Component {
   constructor() {
      super()
      this.createListItems = this.createListItems.bind(this)
   }

   createListItems() {
      return logosPaths.map((logoPath, index, logos) => {
         return (logoPath === meteorImg)
            ?
            <li key={logoPath}>
               <img
                  src={ logoPath }
                  style={styles.meteorImgStyle}
                  key={logoPath + ' ' + index}/>
            </li>
            :
            <li key={logoPath}>
               <img
                  src={ logoPath }
                  key={logoPath + ' ' + index}/>
            </li>
      })
   }

   render() {
      if (window) {
         let windowWidth = $(window).width()
      }

      return (
         <div className="showcase-tech-container container">
            <ul className="brand-logos row centred">
               { this.createListItems() }
            </ul>
            {this.renderMsg}
         </div>
      )
   }

}
