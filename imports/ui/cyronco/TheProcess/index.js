import React from 'react';
import SideImg from '../SideImg';
import CentredText from '../CentredText';

// const style = {
//   h1: {
//     textTransform: 'capitalize',
//     fontWeight: '500',
//     fontSize: '36px',
//     marginBottom: '15px',
//   },

//   small: {
//     fontSize: '15px',
//     lineHeight: '30px',
//   },
// };


// export default class TheProcess extends Component {
export default function TheProcess() {
  return (
    <SideImg
      heading={<CentredText heading="Why Choose Us" icon="icon-linegraph" id="TheProcess" />}
      small="This is some detail about what is actually gonna be happening on the website"
      img="funnelPics.png"
      float="left"
      backgroundColor={'#f77'}
      accordian={true}
      padding="25px 0 20px 0"
      />
  );
}

