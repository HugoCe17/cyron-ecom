
import React, { PropTypes, Component } from 'react'


let styles = {
   container: {
      position: 'absolute',
      background: 'transparent',
   },
   message: {
      fontFamily: 'Futura-pt',
      margin: '1px',
      color: "#fff",
   }

}

export default class Notification extends Component {
   constructor(props) {
      super(props)
      this.state = {}
      this.onLeave = this.onLeave.bind(this)
      this.onEnter = this.onEnter.bind(this)
   }

   onEnter() {
      let $notification = $(this.refs.notification)
      if ($notification.hasClass('fadeOutUp')) {
         $notification.removeClass('fadeOutUp')
            .addClass('fadeInDown')
            .animate({
               height: '20px'
            }, 200)
      }
   }

   onLeave() {
      let $notification = $(this.refs.notification)
      if ($notification.hasClass('fadeInDown')) {
         $notification.removeClass('fadeInDown')
            .addClass('fadeOutUp')
            .animate({
               height: '0px'
            }, 200)
      }
   }

   render() {
      let { message } = this.props
      let { height } = this.state
      return (
         <div
            className="centred animated fadeInDown"
            ref="notification"
            style={ _.extend({}, styles.container, { height }) } >
            <p
               className="text-center animated fadeInDown"
               style={ styles.message }>
               { message }
            </p>
            <Waypoint
               onEnter={ this.onEnter }
               onLeave={ this.onLeave }
               threshold={ 0 } />
         </div>
      )
   }
}

Notification.PropTypes = {
   message: PropTypes.string.isRequired
}