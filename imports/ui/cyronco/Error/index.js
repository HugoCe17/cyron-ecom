export default function Error() {
   document.body.style.paddingLeft = '0'
   return (
      <div className="jumbotron">
         <h1 style={{ textAlign: 'center' }}>Error 404</h1>
         <p style={{ textAlign: 'center' }}>Seems like you're trying to access a page we don't serve...</p>
      </div>
   )
}