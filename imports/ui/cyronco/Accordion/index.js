import React, { Component, PropTypes } from 'react'
import fetchDOMNode from 'react-dom'

/**
 * child for Accordion that houses text
 * 
 * @class Section
 * @extends {Component}
 */
class Section extends Component {
   /**
    * Creates an instance of Section.
    * 
    * @param props (parent props)
    */
   constructor(props) {
      super(props)

      //Prop from Parent is initiated as state
      this.state = {
         open: this.props.open
      }

      //Binding
      this.handleClick = this.handleClick.bind(this)

   }


   /**
    * Uses its ID to identify and change its state in the state array
    * The new state is passed in to the clickHandler passed from Parent
    */
   handleClick() {
      let newState = [false, false, false, false]
      let $section = $(this.refs.section)

      if (!$section.hasClass('open')) {
         newState[this.props.id] = !newState[this.props.id]
         this.setState({ open: !this.state.open })
      }

      this.props.clickhandler(newState)
   }

   /**
    * Note the class is being passed as a prop as the logic is done in the parent component
    * 
    * @returns (JSX)
    */
   render() {
      return (
         <div
            className={ this.props.class }
            ref="section">
            <button>toggle</button>
            <h1
               className="sectionhead"
               onClick={this.handleClick}>
               {this.props.title}
            </h1>
            <div className="articlewrap">
               <div className="article">
                  {this.props.small}
               </div>
            </div>
         </div>
      )
   }
}

/**
 * Parent Component which renders Sections
 * 
 * @export
 * @class CyronAccordion
 * @extends {Component}
 */
export default class CyronAccordion extends Component {

   /**
    * Initializes an array of booleans which represent the sections and if they are closed or open
    * Creates an instance of CyronAccordion.
    * 
    * @param props (parent props)
    */
   constructor(props) {
      super(props)
      this.state = {
         state: [true, false, false, false],
      }

      this.handleClick = this.handleClick.bind(this)
   }

   /**
    * This is passed to the child components to alter state from parent component.
    * 
    * @param newState (description)
    */
   handleClick(newState) {
      this.setState({ state: newState })
   }

   /**
    * (Main Accordion)
    * 
    * @returns (JSX)
    */
   render() {

      return (
         <div className="acc-main">
            <h1 className="title">
               { this.props.title }
            </h1>
            {this.renderSections(this.state.state) }
         </div>
      )
   }

   /**
    * Grabs the indivitual state of a section from the array and returns a class name 
    * 
    * @param state (Boolean[])
    * @param id (select index)
    * @returns (select appropriate class dependending on truthy or falsy value )
    */
   getClass(state, id) {
      return (!state[id]) ? "section" : "section open"
   }

   /**
    * The creation of the sections is made here. This takes state as a param.
    * 
    * @param state (Boolean[])
    * @returns (sections JSX)
    */
   renderSections(state) {
      let dataArr = [
         { id: "0", title: "1. Vision", small: 'As expected, we spend a considerable amount of time understanding your brand & culture. We understand that we will be creating how the world views your business online which is why grasping your vision is our first priority. ' },
         { id: "1", title: "2. Design", small: 'With our knowledge of your potential user audience combined with your vision, we design a user interface based on user experience engineered to hook users onto your website or mobile application.' },
         { id: "2", title: "3. Development", small: 'The meat of our work lies here. This is where our designers and developers work collaboratively and figure out what works best. All of our skills will be put to the test while we bring your vision to life.' },
         { id: "3", title: "4. Launch", small: 'At last we present to you the final product which will never come short of your expectations and with your permission, we will publish your website and/or mobile application.' }
      ]

      //Return the sections with all the props they need to function            
      return dataArr.map((obj) => {
         return (
            <Section
               title={obj.title}
               class={this.getClass(state, obj.id) }
               key={obj.id}
               id={obj.id}
               open={state[obj.id]}
               small={obj.small}
               clickhandler={this.handleClick} />
         )
      })
   }
}
