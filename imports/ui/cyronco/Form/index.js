
import React, {Component, PropType} from 'react'
import TextField from 'material-ui/TextField'
import MenuItem from 'material-ui/MenuItem'
import IconButton from 'material-ui/IconButton'
import AppBar from 'material-ui/AppBar'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'


class Form extends Component {
   constructor(props) {
      super(props)
      this.state = {}



   }


   componentWillMount() {

   }

   componentWillUnmount() {

   }


   onResize() {

   }

   render() {

      return (
         <div className="text-center">
            <div
               className="form-container"
               style={{}}>
               <h1 style={style.h3}>
                  Free Consultation
               </h1>
               <form
                  onSubmit={this._handleSubmit}
                  onChange={this.clearErrors}>
                  <TextField
                     floatingLabelStyle={{ color: "#343e4a" }}
                     underlineFocusStyle={{ borderColor: "rgba(98%, 37.8%, 30.9%, 1)" }}
                     onChange={ (event) => {
                        this.setState({
                           FullName: event.target.value
                        })
                     } }
                     ref="fullName"
                     name="fullName"
                     required={true}
                     type="text"
                     floatingLabelText="Full Name"

                     />
                  <br/>

                  <TextField
                     floatingLabelStyle={{ color: "#343e4a" }}
                     underlineFocusStyle={{ borderColor: "rgba(98%, 37.8%, 30.9%, 1)" }}
                     onChange={ (event) => {
                        this.setState({
                           Email: event.target.value
                        })
                     } }
                     ref="email"
                     name="Email"
                     required={true}
                     type="Email"
                     floatingLabelText="Email"
                     />
                  <br/>
                  <TextField
                     floatingLabelStyle={{ color: "#343e4a" }}
                     underlineFocusStyle={{ borderColor: "rgba(98%, 37.8%, 30.9%, 1)" }}
                     multiLine={false}
                     floatingLabelText="Contact us now!"
                     />
                  <br/>

                  <RaisedButton
                     backgroundColor="#f77"
                     label="Submit"
                     primary={true}
                     style={style.btn} />
               </form>
            </div>
         </div>
      )
   }
}