import React, {Component, PropType} from 'react'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import NavigationClose from 'material-ui/svg-icons/navigation/close'
import Colors from 'material-ui/styles/colors'
import IconMenu from 'material-ui/IconMenu'
import MenuItem from 'material-ui/MenuItem'
import IconButton from 'material-ui/IconButton'
import AppBar from 'material-ui/AppBar'

const brandLogo = './images/cyron-logo.png'
const altBrandLogo = './images/cyron-logo-alt.png'


const style = {
      appbar: {
            background: "dodgerblue",
            position: "fixed"
      },

      title: {
            color: "white",
            fontWeight: "200",
            letterSpacing: "8px"
      },

      cyronBrand: {
            paddingTop: '10px',
            transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
            margin: '-34px 0px -3px 11px'
      }
}

export default class Navbar extends Component {
      constructor(props) {
            super(props)

            this.state = {
                  appBg: 'dodgerblue',
                  brandHeight: '120px',
                  brandLeft: 0,
                  barHeight: '110px',
                  depth: 0,
                  iconBtnRight: 0,
                  iconTop: 0,
                  iconColor: '#fff',
                  iconHeight: '34px',
                  iconWidth: '34px',
                  iconRight: '15px',
                  titleColor: '#fff',
                  brandLogo: altBrandLogo
            }

            this.handleScroll = this.handleScroll.bind(this)
            this.onClick1 = this.onClick1.bind(this)
            this.onClick2 = this.onClick2.bind(this)
            this.onClick3 = this.onClick3.bind(this)

      }


      componentDidMount() {
            window.addEventListener('scroll', this.handleScroll)
      }
      
      componentWillUnmount() {
            if (isBrowser()) {
                  window.removeEventListener('scroll', this.handleScroll)
            }
      }

      render() {
            /** With Redux, this will be from this.props */
            let {
                  appBg,
                  barHeight,
                  brandHeight,
                  depth,
                  iconBtnRight,
                  iconColor,
                  iconHeight,
                  iconTop,
                  iconWidth,
                  iconRight,
                  titleColor,
                  brandLogo
            } = this.state


            return (
                  <div>
                        <AppBar
                              className="nav"
                              style={
                                    _.extend({}, style.appbar, {
                                          background: appBg,
                                          height: barHeight
                                    })
                              }
                              titleStyle={ _.extend({}, style.title, { color: titleColor }) }
                              zDepth={ depth }
                              iconElementLeft={
                                    <img
                                          className=""
                                          ref='brandImg'
                                          src={brandLogo}
                                          style={_.extend({}, style.cyronBrand, {
                                                position: 'relative',
                                                height: brandHeight,
                                                left: this.state.brandLeft

                                          })
                                          }/>
                              }
                              />
                  </div>
            )
      }

      getIconMenu() {
            return (
                  <IconMenu
                        iconButtonElement={
                              <IconButton
                                    style={{
                                          right: iconBtnRight
                                    }}
                                    iconStyle={{
                                          position: 'relative',
                                          transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
                                          fill: iconColor,
                                          height: iconHeight,
                                          width: iconWidth,
                                          top: iconTop,
                                    }} >
                                    <MoreVertIcon  />
                              </IconButton>
                        }
                        targetOrigin={{ horizontal: 'right', vertical: 'top' }}
                        anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
                        >
                        <MenuItem onClick={this.onClick2} primaryText="Our Work" />
                        <MenuItem onClick={this.onClick1} primaryText="Packages" />
                        <MenuItem onClick={this.onClick2} primaryText="Get Started" />
                  </IconMenu>
            )
      }

      onClick1() {
            $(".packages").scrollTo($('nav').height())
      }

      onClick2() {
            $(".showcase").scrollTo($('nav').height())
      }

      onClick3() {
            $(".packages").scrollTo($('nav').height())
      }

      handleScroll(e) {
            let scrollTop = window ? window.scrollY : e.srcElement.body.scrollTop

            if (scrollTop > 72)
                  this.setState({ depth: 2 })
            else
                  this.setState({ depth: 0 })

            /* Passed Video */
            return (scrollTop >= ($('section').height() - 30))
                  ? this.setState(
                        {
                              appBg: "#fff",
                              barHeight: '67px',
                              brandHeight: '100px',
                              brandLeft: '10px',
                              brandLogo: brandLogo,
                              iconBtnRight: '18px',
                              iconColor: '#000',
                              iconTop: '5px',
                              iconHeight: '24px',
                              iconWidth: '24px',
                              iconRight: '15px',
                              titleColor: '#000',
                        }
                  )
                  : this.setState({
                        appBg: "transparent",
                        barHeight: '110px',
                        brandHeight: '120px',
                        brandLeft: 0,
                        brandLogo: altBrandLogo,
                        iconBtnRight: 0,
                        iconTop: 0,
                        iconHeight: '34px',
                        iconWidth: '34px',
                        iconColor: '#fff',
                        iconRight: '15px',
                        titleColor: '#fff',
                  })

      }


}