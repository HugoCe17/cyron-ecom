
import React, { Component, PropTypes } from 'react'

const K_WIDTH = 40
const K_HEIGHT = 40

const greatPlaceStyle = {
   // initially any map object has left top corner at lat lng coordinates
   // it's on you to set object origin to 0,0 coordinates
   position: 'absolute',
   width: '100%',
   height: '400px',
   left: -K_WIDTH / 2,
   top: -K_HEIGHT / 2,
   zIndex: 1,
   left: 0,
   top: 0,
   index: 10,
   border: '5px solid #f44336',
   borderRadius: K_HEIGHT,
   backgroundColor: 'white',
   textAlign: 'center',
   color: '#3f51b5',
   fontSize: 16,
   fontWeight: 'bold',
   padding: 4
}

export default class CyronHQ extends Component {
   constructor(props) {
      super(props);
   }

   render() {
      return (
         <div style={ greatPlaceStyle }>
            { this.props.text }
         </div>
      );
   }
}

CyronHQ.propTypes = {
   text: PropTypes.string
};
CyronHQ.defaultProps = {};   