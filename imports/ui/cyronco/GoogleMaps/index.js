import React, { PropTypes, Component } from 'react';
import GoogleMap from 'google-map-react';
import CyronHQ from './CyronHQ';
import CentredText from '../CentredText';

const styles = {
  container: { background: '#fff' },
  map: { display: 'block', position: 'relative', width: '100%', height: '48vh' },
};

export default class GoogleMaps extends Component {
  createMapOptions(maps) {
    return {
      draggable: false,
      panControl: false,
      mapTypeControl: false,
      scrollwheel: false,
      styles: [{
        stylers: [
          { saturation: 0 },
          { gamma: 0.8 },
          { lightness: 4 },
          { visibility: 'on' },
        ],
      }],
    };
  }

  render() {
    return (
      <div style={styles.container}>
        <CentredText
          heading="Our Location"
          icon="icon-linegraph"
          id="google-maps"
        />
        <GoogleMap
          style={styles.map}
          options={this.createMapOptions}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
          bootstrapAPIKey={{ key: 'AIzaSyCWPVEGiNhiczeGDvWO2YZwcm6g6PHySk4' }}
        >
          <CyronHQ {...this.props.cyronHQ} text={'Cyron'} /* road circle */ />
        </GoogleMap>
      </div>
    );
  }
}

GoogleMaps.defaultProps = {
  center: { lat: 25.830947, lng: -80.310090 },
  zoom: 11,
  cyronHQ: { lat: 59.724465, lng: 30.080121 },
};
