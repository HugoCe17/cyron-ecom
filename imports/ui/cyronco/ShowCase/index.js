import React, {Component, PropType} from 'react'
import Slider from 'react-slick'
// import CenteredText from '../CenteredText'

const styles = {
    main: {
        positition: 'relative',
        background: "white",
        width: "80%",
        padding: "10px"
    },
    h1: {
        textTransform: "capitalize",
        fontWeight: "500",
        fontSize: "60px",
        marginBottom: "15px",
        paddingTop: "10px"
    },

    wrapper: {
        width: "100vw",
        background: "white"
    }
}

export default class ShowCase extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        if (typeof window !== 'undefined') {
            window.setTimeout(() => window.innerWidth + 1, 100)
        }
    }

    renderImages() {

        return this.props.data.map((string) => {
            return <img key={string} className="img-responsive" src={string} />
        })
    }

    render() {
        let settings = {
            arrows: this.props.arrows,
            dots: this.props.dots,
            speed: 700,
            slidesToShow: this.props.show,
            slidesToScroll: 1,
            autoplay: this.props.auto,
            autoplaySpeed: 2000,
            adaptiveHeight: false
        }

        return (
            <div
                className="ss-style-doublediagonal"
                style={styles.wrapper}>
                <div
                    className="container showcase"
                    ref="slide"
                    style={ styles.main }>
                    <div style={{ marginBottom: "50px" }} className="row">
                        <h1
                            className='text-center'
                            style={styles.h1}>Our Work</h1>
                        <Slider {...settings}>
                            {this.renderImages() }
                        </Slider>
                    </div>
                </div>
            </div>

        )
    }

}
