/**
 * Author: Hugo Cedano
 */
import React, { Component, PropTypes } from 'react'
import { Parallax, Background } from 'react-parallax'


const styles = {
    bgImgHeight: '400px'
}

export default class CyronParallax extends Component {

    constructor(props) {
        super(props)
        this.state = {
            bgMargins: 0
        }
    }


    componentWillMount() {
        this.setState({
            bgMargins: this.props.bgMargins
        })
    }


    componentDidMount() {
        if (document || window)
            $('.react-parallax-content').height(600)
    }

    render() {
        return (
            <Parallax strength={300}>
                <Background>
                    <img
                        style={{
                            margin: this.state.bgMargins,
                            width: 'auto',
                            height: 'auto'
                        }}
                        src={this.props.bgImage} />

                </Background>
            </Parallax>
        )
    }
}