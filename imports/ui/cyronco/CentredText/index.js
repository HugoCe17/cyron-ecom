import React, { Component, PropTypes } from 'react';
import $ from 'jquery'

(typeof window !== 'undefined')
  ? require('waypoints/lib/noframework.waypoints.min')
  : null;

const style = {
  container: {
    background: 'white',
    paddingTop: '80px',
    
  },

  h1: {
    textTransform: 'capitalize',
    fontWeight: '500',
    fontSize: '50px',
    marginBottom: '15px',
    color: '#4F5692',
  },

  small: {
    fontSize: '20px',
    lineHeight: '30px',
    color: '#4F5692',
  },

  icon: {
    color: '#FF7777',
    fontSize: '30px',
    marginBottom: '5px',
  },
};

// export default function CenteredText({icon, id, heading, small}) {
class CentredText extends Component {

  constructor(props) {
    super(props);
    this.state = {
      animation: '',
    };

    this.onEnter = this.onEnter.bind(this);
  }

  componentDidMount() {
    const { id } = this.props;
    const centredTxtEl = document.getElementsByClassName(`centred-text-wp ${id}`)[0];
    const wp = new Waypoint({
      element: centredTxtEl,
      handler: this.onEnter,
      offset: 'bottom-in-view',
    });
  }

  onEnter() {
    this.setState({
      animation: 'fadeInDown',
    });
  }

  render() {
    let { id, small, heading, icon } = this.props;
    return (
      <div id={id} className="row" style={style.container}>
        <div
          className={`container text-center animated ${this.state.animation}`}
          style={{ opacity: 0 }}
        >
          <i style={style.icon} className={icon}></i>
          <h1 style={style.h1}>{heading}</h1>
          <small style={style.small}>{small}</small>
          <hr className="style17" />
        </div>
        <div className={`centred-text-wp ${id}`} />
      </div>
    );
  }
}

CentredText.propTypes = {
  id: PropTypes.string.isRequired,
  heading: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  small: PropTypes.string,
};

export default CentredText;

