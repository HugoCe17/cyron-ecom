import React from 'react'
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

styles = {
  cardContainer: {
    position: 'relative',
    float: 'left',
    width: '300px',
    height: 'auto',
  },
  card: {
    width: '300px',
    height: 'auto',
  },
}
// let temp = (

//     <CardHeader
//       title="URL Avatar"
//       subtitle="Subtitle"
//       avatar="http://lorempixel.com/100/100/nature/"
//       />


// )

const Product = ({title, description, price, image}) => (
  <Card
    containerStyle={styles.cardContainer}
    style={styles.card}>
    <CardMedia
      overlay={<CardTitle title={title} subtitle="Overlay subtitle" />}
      >
      <img className="img-responsive" src={image} />
    </CardMedia>
    <CardText>
      {description}
    </CardText>
    <CardActions>
      <FlatButton label="Buy" />
      <FlatButton label="Tell Me More" />
    </CardActions>
  </Card>
);

export default Product;