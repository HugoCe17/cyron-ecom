/** server-render clause for client-side loading only */
typeof window !== 'undefined' ? require('jquery') : null
typeof window !== 'undefined' ? require('waypoints/lib/noframework.waypoints.min') : null;

import React, { Component, PropTypes } from 'react';
import { GridList, GridTile } from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    opacity: 0,
    animationDelay: '0.2s',
    WebkitAnimationDelay: '0.2s',
  },
  gridList: {
    width: 500,
    height: 'auto',
    overflowY: 'auto',
    marginBottom: 24,
  },
};

class GridedProducts extends Component {

  constructor(props) {
    super(props)
    this.state = {
      products: [],
      animation: ''
    }

    this.onEnter = this.onEnter.bind(this)
    this.fetchAllProducts = this.fetchAllProducts.bind(this)
    this.renderGridedProducts = this.renderGridedProducts.bind(this)
  }

  componentWillMount() {
    this.fetchAllProducts()
  }

  componentDidMount() {
    const { id } = this.props;
    const centredTxtEl = document.getElementsByClassName(`gridlist-wp ${id}`)[0];
    const wp = new Waypoint({
      element: centredTxtEl,
      handler: this.onEnter,
      offset: 'bottom-in-view',
    });
    $('.gridlist').css({
      display: 'flex'
    })
  }

  onEnter() {
    this.setState({
      animation: 'fadeIn'
    })
  }

  renderGridedProducts() {
    return this.state.products.map((product) => (
      <GridTile
        className={`animated ${this.state.animation}`}
        key={product.product_id}
        title={product.title}
        actionPosition="left"
        titlePosition="top"
        titleBackground="linear-gradient(to bottom, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.3) 70%,rgba(0,0,0,0) 100%)"
        cols={product.featured ? 2 : 1}
        rows={product.featured ? 2 : 1}
        >
        <img src={product.images[0].src} />
      </GridTile>
    ))
  }

  async fetchAllProducts() {
    let products = await Meteor.callPromise('fetchAllProducts')
    return products.map((product) =>
      (this.setState({
        products: this.state.products.concat(product.attrs)
      }))
    )
  }

  render() {
    let { id } = this.props
    return (
      <div
        style={styles.root}
        className={`container animated ${this.state.animation}`
      }>
        <GridList
          className="gridlist"
          cellHeight={200}
          style={styles.gridList}
        >
          {this.renderGridedProducts()}
        </GridList>
        <div className={`gridlist-wp ${id}`} />
      </div>
    )
  }
}

GridedProducts.propTypes = {
  id: PropTypes.string.isRequired
}
GridedProducts.defaultProps = {
  id: ''
}

export default GridedProducts;