import { Meteor } from 'meteor/meteor';
import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import ProductCollection from '../../../api/products/products'
import ShopifyBuy from 'shopify-buy'

import Product from '../Product'


const ShopClient = ShopifyBuy.buildClient({
  apiKey: '22f8cf2b78da26bc3b9eec488a180f12',
  myShopifyDomain: 'just-stories',
  appId: '6',
});

export default class Products extends Component {

  constructor(props) {
    super(props)
    this.state = {
      products: []
    }

    this.fetchAllProducts = this.fetchAllProducts.bind(this)
    this.renderProducts = this.renderProducts.bind(this)
  }


  componentWillMount() {
    this.fetchAllProducts()
  }


  componentDidMount() {
    /** get all products into the state*/

  }

  renderProducts() {
    return this.state.products.map((_product) => (
      <div
        key={_product.product_id}
        className="col-md-3 col-md-offset-1"
        sytle={{ float: 'none', margin: '0 auto' }}>
        <Product
          title={_product.title}
          description={_product.description}
          image={'http://lorempixel.com/300/300/food/'}
          />
      </div>
    ))
  }

  async fetchAllProducts() {
    let products = await Meteor.callPromise('fetchAllProducts')
    return products.map((product) =>
      (this.setState({
        products: this.state.products.concat(product.attrs)
      }))
    )
  }

  render() {
    return (
      <div className="container" style={{ position: 'relative' }}>
        <div className="row">
          {this.renderProducts() }
        </div>
      </div>
    )
  }
}