// import '../imports/startup/'
import $ from 'jquery'

if (Meteor.isClient) {
  $.fn.scrollTo = function (offset) {
    return this.each(function () {
      const root = $('html, body');
      root.stop().animate({
        scrollTop: $(this).offset().top - offset,
      }, 1000, 'swing');
    });
  };
}
