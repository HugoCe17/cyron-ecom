import React from 'react'
import { Meteor } from 'meteor/meteor'
import { FlowRouter } from 'meteor/kadira:flow-router-ssr';
import { mount } from 'react-mounter';
import AppHelmet from '../imports/ui/layouts/AppHelmet/AppHelmet'
import App from '../imports/ui/layouts/App/App'
import Layout from '../imports/ui/layouts'


FlowRouter.route('/', {
  action() {
    mount(Layout, {
      content: (<App />),
    });
  },
});
